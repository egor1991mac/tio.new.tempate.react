import React from 'react';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux'

const Nav = ({links,HandleClick}) => {
        console.log(links);
        if(links)
            return(
              <nav>
                  {
                      links.map(item=>
                          <Button href={item.hasOwnProperty('href') && item.href}  key={Math.random()} onClick={()=>HandleClick(item)}>
                              {item.name}
                          </Button>
                      )
                  }
              </nav>
          )
    else return null;

};


export default connect(
    state => ({
        links:state.nav
    }),
    dispatch => ({
        HandleClick: (data)=> dispatch({type:"SELECT_NAV", payload:data})
    })
)(Nav);