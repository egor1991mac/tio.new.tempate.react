import React from 'react';
import Appbar from '../../components/appbar';
import Logo from '../../components/logo';
import Nav from '../../components/nav';

const Header = ({ }) => {
    return (
       <>
            <Appbar>
                <Logo key={'logo'}/>
                <Nav key ={'nav'} links={[{href:"#",name:'блог'}]}/>
            </Appbar>
      </>
    );
};

export default Header;

