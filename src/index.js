import React from 'react';
import ReactDOM from 'react-dom';
import App from './pages/index.js';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
window.ReduxObject = {};

setTimeout(()=>{

    Object.defineProperty(window,'ReduxObject', {
        set:(val)=> {
            if(val != undefined){
                const rootElement = document.getElementById("root");
                const redux = val.redux;
                const storConfig = val.stor;
                const stor = redux.createStore(storConfig.Navigation, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
                if (rootElement.hasChildNodes()) {
                    ReactDOM.hydrate(
                        <Provider store={stor}>
                            <App />
                        </Provider>
                        , rootElement);
                } else {
                    ReactDOM.render(<App />, rootElement);
                }
            }
            serviceWorker.unregister()
        }
    });
},300);



