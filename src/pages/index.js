import React, {Suspense, lazy} from 'react';
import './index.css';

const Header = lazy(()=>import('../layout/header'));

const Index = () => {
    return (
        <div>
            <Suspense fallback = {<div>...loading...</div>}>
                <Header/>
            </Suspense>
        </div>
    );
};

export default Index;