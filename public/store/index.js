const initialState = [{
    href: '#',
    name: 'link1',
    },
    {
        href: '#',
        name: 'link2',
    }
];

function Navigation(state = {nav:initialState}, action) {
    if (action.type === 'SELECT_NAV') {
        window.location.href = action.payload.href;
    }
    return state;
};